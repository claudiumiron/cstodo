//
//  ListViewController.swift
//  CSToDo
//
//  Created by Claudiu Miron on 07/04/2020.
//  Copyright © 2020 Claudiu Miron. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var itemService: ItemService?
    
    var items: [ToDoItem?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let coreDataService = (UIApplication.shared.delegate as! AppDelegate).coreDataService
        
        itemService = ItemService(coreDataService: coreDataService)
        
        itemService!.fetchItems { (items) in
            self.items = items
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.systemBlue
    }
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        let toDoVC = segue.destination as! ToDoViewController
        let indexPath =
            tableView.indexPath(for: sender as! UITableViewCell)!
        toDoVC.toDo = items[indexPath.row]
    }
}

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let todo = items[indexPath.row]
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
        cell.textLabel?.text = todo!.content
        cell.detailTextLabel?.text = nil
        
        var color: UIColor
        
        switch todo?.priority {
        case .low:
            color = AppColors.lowPriority
            
        case .medium:
            color = AppColors.mediumPriority
            
        case .high:
            color = AppColors.highPriority
        case .none:
            color = UIColor.black
        }
        
        cell.textLabel!.textColor = color
        cell.tintColor = color
        
        return cell
    }
}

extension ListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        if let text = searchBar.text {
            print("Edited. \(text)")
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchBar.resignFirstResponder()
        print("done editing")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        //searchBar.resignFirstResponder()
    }
}
