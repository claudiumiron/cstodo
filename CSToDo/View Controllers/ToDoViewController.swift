//
//  ToDoViewController.swift
//  CSToDo
//
//  Created by Claudiu Miron on 07/04/2020.
//  Copyright © 2020 Claudiu Miron. All rights reserved.
//

import UIKit

class ToDoViewController: UIViewController {
    
    public var toDo: ToDoItem?
    
    @IBOutlet weak var checkButton: UIBarButtonItem!
    @IBOutlet private weak var priorityButton: UIBarButtonItem!
    @IBOutlet private weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = toDo?.content
        
        var color: UIColor?
        
        switch toDo?.priority {
        case .low:
            color = AppColors.lowPriority
            
        case .medium:
            color = AppColors.mediumPriority
            
        case .high:
            color = AppColors.highPriority
        case .none:
            color = UIColor.black
        }
        
        self.navigationController?.navigationBar.tintColor = color!
    }
}
