//
//  AppColors.swift
//  CSToDo
//
//  Created by Claudiu Miron on 07/04/2020.
//  Copyright © 2020 Claudiu Miron. All rights reserved.
//

import UIKit

class AppColors {
    static var lowPriority: UIColor {
        return UIColor.systemPurple
    }
    
    static var mediumPriority: UIColor {
        return UIColor.systemIndigo;
    }
    
    static var highPriority: UIColor {
        return UIColor.systemOrange
    }
}
