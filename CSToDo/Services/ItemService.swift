//
//  ItemService.swift
//  CSToDo
//
//  Created by Claudiu Miron on 07/04/2020.
//  Copyright © 2020 Claudiu Miron. All rights reserved.
//

import Foundation

class ItemService {
    
    let coreDataService: CoreDataService
    
    init(coreDataService: CoreDataService) {
        self.coreDataService = coreDataService
    }
    
    func fetchItems(completion: ([ToDoItem?]) -> Void) {
        completion( [ToDoItem(id: 0,
                         date: Date().addingTimeInterval(00),
                         completed: false,
                         priority: .high,
                         content: "todo 1 and then some long text here to show that it is truncated in the list screen"),
                    ToDoItem(id: 1,
                         date: Date().addingTimeInterval(15),
                         completed: false,
                         priority: .medium,
                         content: "todo 2 and then some long text here to show that it is truncated in the list screen"),
                    ToDoItem(id: 2,
                         date: Date().addingTimeInterval(15),
                         completed: true,
                         priority: .low,
                         content: "todo 3 and then some long text here to show that it is truncated in the list screen")])
    }
    /*
    func fetchItems(completion: ([ToDo?]) -> Void) -> Void {
        coreDataService.fetchItems { (items) in
            completion(items)
        }
    }
    
    func syncItems() {
        
    }
     */
}
