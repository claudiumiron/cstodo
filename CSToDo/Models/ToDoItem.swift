//
//  ToDoItem.swift
//  CSToDo
//
//  Created by Claudiu Miron on 07/04/2020.
//  Copyright © 2020 Claudiu Miron. All rights reserved.
//

import Foundation

enum Priority: Int {
    case low, medium, high
}

struct ToDoItem {
    let id: Int
    let date: Date
    let completed: Bool
    let priority: Priority
    let content: String
}
